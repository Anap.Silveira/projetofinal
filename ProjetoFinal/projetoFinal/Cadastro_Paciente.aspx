<%@ Page Title="Cadastro de Paciente" Language="C#" MasterPageFile="~/projetoFinal.Master" AutoEventWireup="true" CodeBehind="Cadastro_Paciente.aspx.cs" Inherits="projetoFinal.WebForm6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" >
    <link rel="stylesheet" type="text/css" href="styles.css" >
    <link href="css/style_2.css" rel="stylesheet" type="text/css" />

    <div id="doc" class="yui-t2"> 
        <div align="center">

            <asp:Label ID="CadCli_title" runat="server" align="center" Text="Novo Cadastro - Cliente" BackColor="White" BorderColor="White" BorderWidth="10px" Font-Size="15pt"></asp:Label>

        <br>
        <p align="center">
            <asp:Label ID="info_label" runat="server" Text="Informações pessoais" BackColor="White" BorderColor="White" Font-Bold="True"></asp:Label>
        </p>
        <br>

        </div>
        <div align="center">
            <asp:Label ID="name_label" runat="server" Text="Nome Completo:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="name_input" runat="server" CausesValidation="True" OnTextChanged="name_input_TextChanged"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf1" runat="server" ControlToValidate="name_input" ErrorMessage="Campo Necessário" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="cpfP_label" runat="server" Text="CPF:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="cpf_input" runat="server" CausesValidation="True" OnTextChanged="cpf_input_TextChanged" TextMode="Number"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf2" runat="server" ControlToValidate="cpf_input" ErrorMessage="Campo Necessário" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="tel1_label" runat="server" Text="Telefone celular:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="tel1_input" runat="server" CausesValidation="True" TextMode="Number"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf3" runat="server" ControlToValidate="tel1_input" ErrorMessage="Campo Necessário" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="tel2_label" runat="server" Text="Telefone residêncial:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="tel2_input" runat="server" CausesValidation="True" TextMode="Number"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf4" runat="server" ControlToValidate="tel2_input" ErrorMessage="Campo Necessário" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <center>
            <asp:Label ID="mail_label" runat="server" Text="Endereço de e-mail:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="mail_input" runat="server" CausesValidation="True" TextMode="Email"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf12" runat="server" ControlToValidate="mail_input" ErrorMessage="Campo Necessário" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
            </center>
        <br>
        <p align="center">
            <asp:Label ID="enderec_label" runat="server" Text="Endereço pessoal" BackColor="White" BorderColor="White" Font-Bold="True"></asp:Label>
        </p>
        <br>

        <div align="center">
            <asp:Label ID="street_label" runat="server" BackColor="White" BorderColor="White" BorderWidth="6px">Rua/Avenida</asp:Label>
            <asp:TextBox ID="street_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf5" runat="server" ControlToValidate="street_input" ErrorMessage="Campo Necessário" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="num_label" runat="server" Text="Numero" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="num_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf6" runat="server" ControlToValidate="num_input" ErrorMessage="Campo Necessário" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
            <asp:Label ID="lbBairro" runat="server" Text="Bairro: "></asp:Label>
            <asp:TextBox ID="TbBairro" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Rf9" runat="server" ControlToValidate="TbBairro" EnableClientScript="False" ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="city_label" runat="server" Text="Cidade" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="city_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf7" runat="server" ControlToValidate="city_input" ErrorMessage="Campo Necessário" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
            <asp:Label ID="uf_label" runat="server" Text="Estado" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:DropDownList ID="uf_dd" runat="server" CausesValidation="True">
                <asp:ListItem>AC</asp:ListItem>
                <asp:ListItem>AL</asp:ListItem>
                <asp:ListItem>AP</asp:ListItem>
                <asp:ListItem>AM</asp:ListItem>
                <asp:ListItem>BA</asp:ListItem>
                <asp:ListItem>CE</asp:ListItem>
                <asp:ListItem>DF</asp:ListItem>
                <asp:ListItem>ES</asp:ListItem>
                <asp:ListItem>GO</asp:ListItem>
                <asp:ListItem>MA</asp:ListItem>
                <asp:ListItem>MT</asp:ListItem>
                <asp:ListItem>MS</asp:ListItem>
                <asp:ListItem>MG</asp:ListItem>
                <asp:ListItem>PA</asp:ListItem>
                <asp:ListItem>PB</asp:ListItem>
                <asp:ListItem>PR</asp:ListItem>
                <asp:ListItem>PE</asp:ListItem>
                <asp:ListItem>PI</asp:ListItem>
                <asp:ListItem>RJ</asp:ListItem>
                <asp:ListItem>RN</asp:ListItem>
                <asp:ListItem>RS</asp:ListItem>
                <asp:ListItem>RO</asp:ListItem>
                <asp:ListItem>RR</asp:ListItem>
                <asp:ListItem>SC</asp:ListItem>
                <asp:ListItem>SP</asp:ListItem>
                <asp:ListItem>SE</asp:ListItem>
                <asp:ListItem>TO</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div align="center">
            <asp:Label ID="cep_label" runat="server" Text="CEP" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="cep_input" runat="server" OnTextChanged="cepP_input_TextChanged" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf8" runat="server" ControlToValidate="cep_input" ErrorMessage="Campo Necessário" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
            <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Buscar CEP" />
        </div>

        <br>
        <p align="center">
            &nbsp;</p>
        <br>

        </div>
        <div align="center">
        </div>
        <div align="center">
        </div>
        <div align="center">
        </div>
        <div align="center">
        </div>

    <br>
    <div>
        <asp:Button ID="submitP_btn" runat="server" Text="Cadastrar" BorderWidth="5px" BackColor="#CCCCCC" BorderColor="#CCCCCC" BorderStyle="Outset" Font-Bold="True" Font-Size="12pt" OnClick="submitP_btn_Click" />
    </div>
    <br>

    <div>
        <asp:Label ID="LbAlerta" runat="server" Font-Size="Medium" ForeColor="#FF3300"></asp:Label>
    </div>
</asp:Content>











