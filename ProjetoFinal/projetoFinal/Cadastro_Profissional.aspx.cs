﻿using RestSharp;
using RestSharp.Serialization.Json;
using System;
using System.Windows.Forms;

namespace projetoFinal
{
    public partial class WebForm8 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void espec_input_TextChanged(object sender, EventArgs e)
        {

        }

        protected void submitP_btn_Click(object sender, EventArgs e)
        {
            var connString = "Server=den1.mysql5.gear.host;Database=clinicatours;Uid=clinicatours;Password=Tj5t_EW-Ov8y";
            var connection = new MySql.Data.MySqlClient.MySqlConnection(connString);
            var command = connection.CreateCommand();


            try
            {
                connection.Open();

                if (rf1.IsValid && rf2.IsValid && rf3.IsValid && rf4.IsValid && rf5.IsValid && rf6.IsValid && rf7.IsValid && rf8.IsValid && rf9.IsValid && rf10.IsValid && RfBairro.IsValid && rf11.IsValid && rf12.IsValid && rf14.IsValid)
                {

                    command.CommandText = "INSERT INTO tb_cidade_est (nome , estado) VALUES ('" + cityP_input.Text + "' , '" + ufP_dd.SelectedValue + "'); " +
                        "INSERT INTO tb_bairro(nome , id_cidade_est) VALUES('" + tbBairro.Text + "', LAST_INSERT_ID()); " +
                        "iNSERT INTO tb_cep (cep , id_bairro) VALUES ('" + cepP_input.Text + "' , LAST_INSERT_ID());" +
                        " INSERT INTO tb_logradouro (rua, numero , id_cep) VALUES ('" + streetP_input.Text + "' , '" + numP_input.Text + "',LAST_INSERT_ID()); " +
                        "INSERT INTO tb_medico (nome, cpf, tel_celular , tel_residencial,crm,formacao,especialidade,horario_trabalho,id_logradouro) VALUES ('" + nameP_input.Text + " ' ,' " + cpfP_input.Text + "' ,'" + tel1P_input.Text + "', '" + tel2P_input.Text + "', '" + crm_input.Text + "','" + form_input.Text + "' , '" + espec_input.Text + "', '" + shift_dd.SelectedValue + "', LAST_INSERT_ID());" +
                         "INSERT INTO tb_login(login, senha, id_medico) VALUES (' " + mailP_input.Text + "' ,' " + passwordP_input.Text + "', LAST_INSERT_ID())";
;
                    command.ExecuteNonQuery();
                    LbAlerta.Text = "Dados inseridos com sucesso";
                    Response.Redirect("Doutor.aspx");
                }


            }

            catch (Exception ex)
            {
                LbAlerta.Text = "Não foi possivel estabelecer conexão. \n" + ex.Message;
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }

        protected void streetP_input_TextChanged(object sender, EventArgs e)
        {

        }

        protected void cepS_btn_Click(object sender, EventArgs e)
        {
            RestClient client = new RestClient(string.Format("https://viacep.com.br/ws/{0}/json/", cepP_input.Text));
            RestRequest request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            DadosRetorno dadosRetorno = new JsonDeserializer().Deserialize<DadosRetorno>(response);
            streetP_input.Text = dadosRetorno.logradouro;
            cityP_input.Text = dadosRetorno.localidade;
            tbBairro.Text = dadosRetorno.bairro;
            ufP_dd.Text = dadosRetorno.uf;
        }
    }

    class DadosRetorno
    {
        public string cep { get; set; }
        public string logradouro { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string localidade { get; set; }
        public string uf { get; set; }
        public string unidade { get; set; }
        public string ibge { get; set; }
        public string gia { get; set; }
    }
}
