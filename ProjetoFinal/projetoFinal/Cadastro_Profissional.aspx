<%@ Page Title="Cadastro de profissional" Language="C#" MasterPageFile="~/projetoFinal.Master" AutoEventWireup="true" CodeBehind="Cadastro_Profissional.aspx.cs" Inherits="projetoFinal.WebForm8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" >
    <link rel="stylesheet" type="text/css" href="styles.css" >
    <link href="css/style_2.css" rel="stylesheet" type="text/css" />

    <div id="doc" class="yui-t2"> 
        <div align="center">

            <asp:Label ID="CadPro_title" runat="server" align="center" Text="Novo Cadastro -Profissional" BackColor="White" BorderColor="White" BorderWidth="10px" Font-Size="15pt"></asp:Label>

        <br>
        <p align="center">
            <asp:Label ID="infoP_lbl" runat="server" Text="Informa��es pessoais" BackColor="White" BorderColor="White" Font-Bold="True"></asp:Label>
        </p>
        <br>

        </div>
        <div align="center">
            <asp:Label ID="nameP_lbl" runat="server" Text="Nome Completo:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox runat="server" CausesValidation="True" ID="nameP_input"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf1" runat="server" ControlToValidate="nameP_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="cpfP_lbl" runat="server" Text="CPF:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="cpfP_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf2" runat="server" ControlToValidate="cpfP_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="tel1P_lbl" runat="server" Text="Telefone celular:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="tel1P_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf3" runat="server" ControlToValidate="tel1P_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="tel2P_lbl" runat="server" Text="Telefone resid�ncial:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="tel2P_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf4" runat="server" ControlToValidate="tel2P_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>

        <br>
        <p align="center">
            <asp:Label ID="info_prof" runat="server" Text="Informa��es profissionais" BackColor="White" BorderColor="White"></asp:Label>
        </p>
        <br>

        </div>
        <div align="center">
            <asp:Label ID="crm_lbl" runat="server" Text="CRM:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="crm_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf5" runat="server" ControlToValidate="crm_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="form_lbl" runat="server" Text="Forma��o" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="form_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf6" runat="server" ControlToValidate="form_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="espc_lbl" runat="server" Text="Especialidade" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="espec_input" runat="server" CausesValidation="True" OnTextChanged="espec_input_TextChanged"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf7" runat="server" ControlToValidate="espec_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="shift_lbl" runat="server" Text="Per�odo de atividades" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:DropDownList ID="shift_dd" runat="server">
                <asp:ListItem>Matutino</asp:ListItem>
                <asp:ListItem>Vespertino</asp:ListItem>
                <asp:ListItem>Noturno</asp:ListItem>
            </asp:DropDownList>
        </div>

        <br>
        <p align="center">
            <asp:Label ID="enderecP_lbl" runat="server" Text="Endere�o pessoal" BackColor="White" BorderColor="White"></asp:Label>
        </p>
        <br>

        <div align="center">
            <asp:Label ID="streetP_lbl" runat="server" BackColor="White" BorderColor="White" BorderWidth="6px">Rua/Avenida</asp:Label>
            <asp:TextBox ID="streetP_input" runat="server" CausesValidation="True" OnTextChanged="streetP_input_TextChanged"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf8" runat="server" ControlToValidate="streetP_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="numP_lbl" runat="server" Text="Numero" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="numP_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf9" runat="server" ControlToValidate="numP_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="ufP_lbl" runat="server" Text="Estado" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:DropDownList ID="ufP_dd" runat="server">
                <asp:ListItem>AC</asp:ListItem>
                <asp:ListItem>AL</asp:ListItem>
                <asp:ListItem>AP</asp:ListItem>
                <asp:ListItem>AM</asp:ListItem>
                <asp:ListItem>BA</asp:ListItem>
                <asp:ListItem>CE</asp:ListItem>
                <asp:ListItem>DF</asp:ListItem>
                <asp:ListItem>ES</asp:ListItem>
                <asp:ListItem>GO</asp:ListItem>
                <asp:ListItem>MA</asp:ListItem>
                <asp:ListItem>MT</asp:ListItem>
                <asp:ListItem>MS</asp:ListItem>
                <asp:ListItem>MG</asp:ListItem>
                <asp:ListItem>PA</asp:ListItem>
                <asp:ListItem>PB</asp:ListItem>
                <asp:ListItem>PR</asp:ListItem>
                <asp:ListItem>PE</asp:ListItem>
                <asp:ListItem>PI</asp:ListItem>
                <asp:ListItem>RJ</asp:ListItem>
                <asp:ListItem>RN</asp:ListItem>
                <asp:ListItem>RS</asp:ListItem>
                <asp:ListItem>RO</asp:ListItem>
                <asp:ListItem>RR</asp:ListItem>
                <asp:ListItem>SC</asp:ListItem>
                <asp:ListItem>SP</asp:ListItem>
                <asp:ListItem>SE</asp:ListItem>
                <asp:ListItem>TO</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div align="center">
            <asp:Label ID="cityP_lbl" runat="server" Text="Cidade" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="cityP_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf10" runat="server" ControlToValidate="cityP_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
            <asp:Label ID="lbBairro" runat="server" Text="Bairro:"></asp:Label>
            <asp:TextBox ID="tbBairro" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RfBairro" runat="server" ControlToValidate="tbBairro" EnableClientScript="False" ErrorMessage="RequiredFieldValidator">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="cepP_lbl" runat="server" Text="CEP" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="cepP_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf11" runat="server" ControlToValidate="cepP_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
            <asp:Button ID="cepS_btn" runat="server" OnClick="cepS_btn_Click" Text="Buscar CEP" />
        </div>

        <br>
        <p align="center">
            <asp:Label ID="infoPcad_lbl" runat="server" Text="Informa��es Cadastrais" BackColor="White" BorderColor="White" Font-Bold="True"></asp:Label>
        </p>
        <br>

        <div></div>
        <div align="center">
            <asp:Label ID="mailP_lbl" runat="server" Text="Endere�o de e-mail:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="mailP_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf12" runat="server" ControlToValidate="mailP_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="mailPconf_lbl" runat="server" Text="Confirmar e-mail:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="mailPconf_input" runat="server" CausesValidation="True"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rf13" runat="server" ControlToValidate="mailPconf_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="passwordP_lbl" runat="server" Text="Senha" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="passwordP_input" runat="server" CausesValidation="True" MaxLength="8" TextMode="Password">Inserir c�digo</asp:TextBox>
            <asp:RequiredFieldValidator ID="rf14" runat="server" ControlToValidate="passwordP_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>
        <div align="center">
            <asp:Label ID="passwordPconf_lbl" runat="server" Text="Confirmar Senha:" BackColor="White" BorderColor="White" BorderWidth="6px"></asp:Label>
            <asp:TextBox ID="passwordPconf_input" runat="server" CausesValidation="True" TextMode="Password">Inserir c�digo</asp:TextBox>
            <asp:RequiredFieldValidator ID="rf15" runat="server" ControlToValidate="mailPconf_input" ErrorMessage="Campo Necess�rio" ForeColor="#CC0000" EnableClientScript="False">*</asp:RequiredFieldValidator>
        </div>

    <br>
    <div>
        <asp:Button ID="submitP_btn" runat="server" Text="Cadastrar" BorderWidth="5px" BackColor="#CCCCCC" BorderColor="#CCCCCC" BorderStyle="Outset" Font-Bold="True" Font-Size="12pt" OnClick="submitP_btn_Click" />
    </div>
    <br>

    <div>
        <asp:Label ID="LbAlerta" runat="server" Font-Size="Medium" ForeColor="#FF3300"></asp:Label>
    </div>
</asp:Content>











