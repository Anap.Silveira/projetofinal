<%@ Page Title="Lista de profissionais" Language="C#" MasterPageFile="~/projetoFinal.Master" AutoEventWireup="true" CodeBehind="Doutor.aspx.cs" Inherits="projetoFinal.WebForm9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            border-style: solid;
            border-width: 1px;
        }
        .auto-style2 {
            height: 22px;
        }
        </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" >
    <link rel="stylesheet" type="text/css" href="styles.css" >
    <link href="css/style_2.css" rel="stylesheet" type="text/css" />

    <div id="doc" class="yui-t2"> 

        <div align="center">
            <asp:Label ID="Page_title" runat="server" align="center" Text="Cadastro de profissionais" BackColor="White" BorderColor="White" BorderWidth="10px" Font-Size="15pt"></asp:Label>
        </div>
                <div align="center">

            <asp:Label ID="new_lbl" runat="server" BackColor="White" BorderColor="White" BorderWidth="10px" Text="Realizar novo cadastro "></asp:Label>

                    <asp:Button ID="btCadastrar" runat="server" OnClick="btCadastrar_Click" Text="Cadastrar" PostBackUrl="~/Cadastro_Profissional.aspx" />

        </div>

        <div align="center">

            <asp:Label ID="search_lbl" runat="server" Text="Filtro por CRM:" BackColor="White" BorderColor="White" BorderWidth="10px"></asp:Label>
            <asp:TextBox ID="search_input" runat="server" Width="108px"></asp:TextBox>
            <asp:Button ID="search_button" runat="server" Text="Buscar" Height="21px" Width="75px" OnClick="search_button_Click" />

        </div>

        <div>
            <center>
            <asp:GridView ID="GridView1" runat="server" OnSelectedIndexChanged="GridView1_SelectedIndexChanged1" Width="485px">
            </asp:GridView>
                <asp:Button ID="btEditar" runat="server" Text="Editar" OnClick="btEditar_Click" />
                <asp:Button ID="BtExcluir" runat="server" OnClick="BtExcluir_Click" Text="Excluir" />
                <br />
                <br />
                <asp:Label ID="LbAlerta" runat="server" Font-Size="Medium" ForeColor="#FF3300"></asp:Label>
                <br />
                </center>
        </div>
        
    </div>
</asp:Content>
