﻿<%@ Page Title="Novo agendamento" Language="C#" MasterPageFile="~/projetoFinal.Master" AutoEventWireup="true" CodeBehind="agendar.aspx.cs" Inherits="projetoFinal.WebForm7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div align="center">
        <asp:Label ID="lbCabecalho" runat="server" BorderStyle="None" Text="Agendamento de consulta" BackColor="White" BorderColor="#ECF5FF" Font-Bold="True" Font-Names="Roboto Light" Font-Size="X-Large" Font-Strikeout="False" ForeColor="#003366" Height="32px" Width="900px"></asp:Label></center>
    </div>  
    
    <div align="center">
        <br />
        <br />
        <asp:Label ID="lb1" runat="server" Text="Informe o cpf do paciente:"></asp:Label>
        <asp:TextBox ID="tbCodigoPaciente" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Text="Informe  o crm do médico:"></asp:Label>
        <asp:TextBox ID="tbCodigoMedico" runat="server"></asp:TextBox>
        <br />
        <br />
        Razão:<br />
        <asp:TextBox ID="tbRazao" runat="server" Height="30px" Width="208px"></asp:TextBox>
        <br />
        <br />
        <br />
        <asp:Label ID="lbData" runat="server" Text="Data:"></asp:Label>
        <asp:TextBox ID="tbData" runat="server" TextMode="Date" Width="127px"></asp:TextBox>
        <br />
        <asp:Label ID="lbHora" runat="server" Text="Hora: "></asp:Label>
        <asp:TextBox ID="tbHora" runat="server" TextMode="Time" Width="128px"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btSalvar" runat="server" Text="Agendar" OnClick="btSalvar_Click" />
        <br />
        <br />
        <asp:Label ID="LbAlerta" runat="server" Font-Size="Medium" ForeColor="#FF3300"></asp:Label>
        <br />
    </div>

</asp:Content>
