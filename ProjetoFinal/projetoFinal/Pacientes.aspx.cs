﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace projetoFinal
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var connString = "Server=den1.mysql5.gear.host;Database=clinicatours;Uid=clinicatours;Password=Tj5t_EW-Ov8y";
            var connection = new MySqlConnection(connString);
            var ds = new DataSet();

            try
            {
                connection.Open();

                MySqlDataAdapter conexaoAdapter = new MySqlDataAdapter("Select nome AS 'Paciente', telefone_celular AS 'Telefone Celular',  telefone_resid as 'Telefone Residêncial' , cpf AS 'CPF'  , email as 'E-mail 'from tb_paciente ", connection);
                conexaoAdapter.Fill(ds);
                GridView1.DataSource = ds;
                GridView1.DataBind();
                GridView1.RowStyle.HorizontalAlign = HorizontalAlign.Center;
                GridView1.HeaderStyle.ForeColor = Color.DarkBlue;


            }
            catch (Exception ex)
            {
                LbAlerta.Text = "Não foi possivel estabelecer conexão. \n" + ex.Message;
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }

        protected void new_btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Cadastro_Paciente.aspx");
        }

        protected void search_button_Click(object sender, EventArgs e)
        {
            var connString = "Server=den1.mysql5.gear.host;Database=clinicatours;Uid=clinicatours;Password=Tj5t_EW-Ov8y";
            var connection = new MySqlConnection(connString);
            var ds = new DataSet();

            try
            {
                connection.Open();

                MySqlDataAdapter conexaoAdapter = new MySqlDataAdapter("Select nome AS 'Paciente', telefone_celular AS 'Telefone Celular', telefone_resid as 'Telefone Residêncial', cpf AS 'CPF', email as 'E-mail' from tb_paciente where cpf like '%" + search_input.Text + "%' ", connection);
                conexaoAdapter.Fill(ds);
                              
                
                GridView1.DataSource = ds;
                GridView1.DataBind();
                GridView1.RowStyle.HorizontalAlign = HorizontalAlign.Center;
                GridView1.HeaderStyle.ForeColor = Color.DarkBlue;


            }
            catch (Exception ex)
            {
                LbAlerta.Text = "Não foi possivel estabelecer conexão. \n" + ex.Message;
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }

        protected void BtApagar_Click(object sender, EventArgs e)
        {
            var connString = "Server=den1.mysql5.gear.host;Database=clinicatours;Uid=clinicatours;Password=Tj5t_EW-Ov8y";
            var connection = new MySqlConnection(connString);
            var command = connection.CreateCommand();
            try
            {
                connection.Open();

                if (!search_input.Text.Equals(null) || !search_input.Text.Equals(""))
                {
                    command.CommandText = "Delete from tb_paciente where cpf = '" + search_input + "' ";
                    command.ExecuteNonQuery();
                    LbAlerta.Text = "Registro apagado!";
                    Response.Redirect("Paciente.aspx");

                }
                else
                {
                    LbAlerta.Text = "Filtre um dado para deleção!";
                }


            }
            catch (Exception ex)
            {
                LbAlerta.Text = "Não foi possivel estabelecer conexão. \n" + ex.Message;
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }

        protected void BtEditar_Click(object sender, EventArgs e)
        {
            string dados = "EditarPaciente.aspx?&cpf=" + search_input.Text;
            Response.Redirect(dados);
            Response.Redirect("EditarPaciente.aspx");
        }
    }
}