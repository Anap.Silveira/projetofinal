﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace projetoFinal
{
    public partial class WebForm11 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lbBusca.Text = Request.QueryString["cpf"];
        }

        protected void BtAtualizar_Click(object sender, EventArgs e)
        {
            var connString = "Server=den1.mysql5.gear.host;Database=clinicatours;Uid=clinicatours;Password=Tj5t_EW-Ov8y";
            var connection = new MySqlConnection(connString);
            var command = connection.CreateCommand();


            try
            {
                connection.Open();


                if (tbCelular.Text.Length>0 && tbResidencial.Text.Length >0 && tbEmail.Text.Length >0)
                {
                    command.CommandText = " UPDATE tb_paciente SET telefone_celular = '"+ tbCelular.Text+"' , telefone_resid = '"+ tbResidencial.Text +"' , email = '"+tbEmail.Text +"' ";
                    int rowAffected = command.ExecuteNonQuery();

                    if (rowAffected > 0)
                    {
                        LbAlerta.Text = "Dados atualizados";
                        Response.Redirect("Paciente.aspx");
                    }
                    else
                    {
                        LbAlerta.Text = "Os dados não foram atualizado, tente novamente!";
                    }

                }
                
            }

            catch (Exception ex)
            {
                LbAlerta.Text = "Não foi possivel estabelecer conexão. \n" + ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }
    }
}
