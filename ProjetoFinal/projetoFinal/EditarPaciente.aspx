﻿<%@ Page Title="Edição - paciente" Language="C#" MasterPageFile="~/projetoFinal.Master" AutoEventWireup="true" CodeBehind="EditarPaciente.aspx.cs" Inherits="projetoFinal.WebForm11" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <asp:Label ID="lbeditarem" runat="server" Font-Bold="True" Text="Editar dados de: "></asp:Label>
<asp:Label ID="lbBusca" runat="server" Font-Bold="True"></asp:Label>
<br />
<br />
<asp:Label ID="lbCelular" runat="server" Text="Novo telefone celular:"></asp:Label>
<asp:TextBox ID="tbCelular" runat="server"></asp:TextBox>
<br />
<asp:Label ID="lbResidencial" runat="server" Text="Novo telefone residêncial:"></asp:Label>
<asp:TextBox ID="tbResidencial" runat="server"></asp:TextBox>
<br />
<asp:Label ID="lbEmail" runat="server" Text="Novo E-mail: "></asp:Label>
<asp:TextBox ID="tbEmail" runat="server" TextMode="Email"></asp:TextBox>
<br />
<br />
<br />
<asp:Button ID="BtAtualizar" runat="server" OnClick="BtAtualizar_Click" Text="Atualizar" />
    <br />
<br />
    <asp:Label ID="LbAlerta" runat="server" Font-Size="Medium" ForeColor="#FF3300"></asp:Label>
<br />



</asp:Content>
