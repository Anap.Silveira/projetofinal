﻿using RestSharp;
using RestSharp.Serialization.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace projetoFinal
{
    public partial class WebForm6 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void cepP_input_TextChanged(object sender, EventArgs e)
        {

        }

        protected void submitP_btn_Click(object sender, EventArgs e)
        {
            var connString = "Server=den1.mysql5.gear.host;Database=clinicatours;Uid=clinicatours;Password=Tj5t_EW-Ov8y";
            var connection = new MySql.Data.MySqlClient.MySqlConnection(connString);
            var command = connection.CreateCommand();


            try
            {
                connection.Open();

                if (rf1.IsValid && rf2.IsValid && rf3.IsValid && rf4.IsValid && rf5.IsValid && rf6.IsValid && rf7.IsValid && rf8.IsValid && rf12.IsValid)
                {
                   
                    command.CommandText = "INSERT INTO tb_cidade_est (nome , estado) VALUES ('" + city_input.Text + "' , '" + uf_dd.SelectedValue + "'); " +
                        "INSERT INTO tb_bairro(nome , id_cidade_est) VALUES('" + TbBairro.Text + "', LAST_INSERT_ID()); " +
                        "iNSERT INTO tb_cep (cep , id_bairro) VALUES ('" + cep_input.Text + "' , LAST_INSERT_ID());" +
                        " INSERT INTO tb_logradouro (rua, numero , id_cep) VALUES ('" + street_input.Text + "' , '" + num_input.Text + "',LAST_INSERT_ID()); " +
                        "INSERT INTO tb_paciente (nome, cpf, telefone_celular , telefone_resid, email, id_logradouro) VALUES ('" + name_input.Text + " ' ,' " + cpf_input.Text + "' ,'" + tel1_input.Text + "', '" + tel2_input.Text + "', '" + mail_input.Text + "',LAST_INSERT_ID()) ";


                    command.ExecuteNonQuery();
                    LbAlerta.Text = "Dados inseridos com sucesso";
                    Response.Redirect("Pacientes.aspx");
                }


            }

            catch (Exception ex)
            {
                LbAlerta.Text = "Não foi possivel estabelecer conexão. \n" + ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }

        protected void cpf_input_TextChanged(object sender, EventArgs e)
        {

        }

        protected void name_input_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            RestClient client = new RestClient(string.Format("https://viacep.com.br/ws/{0}/json/", cep_input.Text));
            RestRequest request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            DadosRetornoPac dadosRetorno = new JsonDeserializer().Deserialize<DadosRetornoPac>(response);
            street_input.Text = dadosRetorno.logradouro;
            city_input.Text = dadosRetorno.localidade;
            TbBairro.Text = dadosRetorno.bairro;
            uf_dd.Text = dadosRetorno.uf;
        }
    }
    class DadosRetornoPac
    {
        public string cep { get; set; }
        public string logradouro { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string localidade { get; set; }
        public string uf { get; set; }
        public string unidade { get; set; }
        public string ibge { get; set; }
        public string gia { get; set; }
    }
}