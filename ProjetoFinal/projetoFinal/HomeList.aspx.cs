﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace projetoFinal
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var connString = "Server=den1.mysql5.gear.host;Database=clinicatours;Uid=clinicatours;Password=Tj5t_EW-Ov8y";
            var connection = new MySqlConnection(connString);
            var ds = new DataSet();

            try
            {
                connection.Open();

                MySqlDataAdapter conexaoAdapter = new MySqlDataAdapter("SELECT p.nome AS 'Paciente', m.nome AS 'Medico' , con.`data` AS 'Data', con.hora AS 'hora', con.razao AS 'Motivo' from tb_paciente p , tb_medico m , tb_consulta con where con.cpf = p.cpf AND con.crm = m.crm ", connection);
                conexaoAdapter.Fill(ds);
                GridView1.DataSource = ds;
                GridView1.DataBind();
                GridView1.RowStyle.HorizontalAlign = HorizontalAlign.Center;
                GridView1.HeaderStyle.ForeColor = Color.DarkBlue;


            }
            catch (Exception ex)
            {
                LbAlerta.Text = "Não foi possivel estabelecer conexão. \n" + ex.Message;
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                    connection.Close();
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}