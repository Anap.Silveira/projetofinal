﻿<%@ Page Title="Login" Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="projetoFinal.WebForm10" %>

<!DOCTYPE html>
<script runat="server">

    protected void BtLogar_Click(object sender, EventArgs e)
    {

    }
</script>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Tours Medicina</title>
    <style type="text/css">
                #divCenter { 
                 width: 419px;
                 height: 170px;                 
                left: 49%; 
                margin: -130px 0 0 -230px; 
                padding:30px;
                position: absolute; 
                top: 25%; }
    </style>
</head>
<body>
    &nbsp;<div id="divCenter">

        <div>
            <asp:Image ID="Image1" runat="server" Height="105px" ImageUrl="~/images/blue-cross-logo-removebg-preview.png" Width="435px" />
        </div>

    <form id="form1" runat="server" >
       <center><asp:Label ID="lbLogin" runat="server" Font-Bold="True" Font-Names="Roboto Light" Text="Login: " ForeColor="Black"></asp:Label><asp:TextBox ID="TbLogin" runat="server" CausesValidation="True" Font-Names="Roboto Light" Width="263px" OnTextChanged="TbLogin_TextChanged" TextMode="Email"></asp:TextBox>
           <asp:RequiredFieldValidator ID="rfvLogin" runat="server" ControlToValidate="TbLogin" EnableClientScript="False" ErrorMessage="*Informe o login" Font-Bold="True" Font-Names="Roboto Light" Font-Size="XX-Small" ForeColor="#FF3300"></asp:RequiredFieldValidator>
        </center>
        <br />
           <center><asp:Label ID="LbSenha" runat="server" Font-Bold="True" Font-Names="Roboto Light" Text="Senha: " ForeColor="Black"></asp:Label><asp:TextBox ID="TbSenha" runat="server" MaxLength="8" TextMode="Password" Width="262px" CausesValidation="True" ValidateRequestMode="Enabled"></asp:TextBox>
               <asp:RequiredFieldValidator ID="rfvSenha" runat="server" ControlToValidate="TbSenha" EnableClientScript="False" ErrorMessage="*informe a senha" Font-Bold="True" Font-Names="Roboto Light" Font-Size="XX-Small" ForeColor="#FF3300"></asp:RequiredFieldValidator>
        </center>
          <br />
          <center style="height: 75px">
              <asp:Button ID="btEntrar" runat="server" BackColor="#666666" BorderStyle="None" Font-Bold="True" Font-Names="Roboto Light" ForeColor="White" Height="25px" OnClick="btEntrar_Click" style="margin-left: 0px" Text="Entrar" Width="391px" />
              <p></p>
              <asp:Button ID="btEsqueciSenha" runat="server" BackColor="White" Font-Names="Roboto Light" ForeColor="Black" Height="28px" OnClick="btEsqueciSenha_Click" Text="Esqueci minha senha" Width="150px" />
        </center>
            
    </form>
        </div>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p></p>
        <p>
        &nbsp;</p>
    <p>
        <center>
                    <asp:Label ID="LbAlerta" runat="server" Font-Size="Medium" ForeColor="#FF3300"></asp:Label>
        </center>
    </p>
    </body>
</html>
