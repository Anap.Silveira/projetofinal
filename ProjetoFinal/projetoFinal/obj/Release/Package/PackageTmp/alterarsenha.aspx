﻿<%@ Page Title="Alterar senha" Language="C#" AutoEventWireup="true" CodeBehind="alterarsenha.aspx.cs" Inherits="projetoFinal.WebForm4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Esqueci minha senha</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
           <center>
               <asp:Label ID="lbLogin" runat="server" Font-Names="Roboto Light" Font-Bold="True"></asp:Label>
               <br /><br />
               <asp:Label ID="lbNovaSenha" runat="server" Font-Bold="True" Font-Names="Roboto Light" Text="Informe a nova senha: "></asp:Label><asp:TextBox ID="tbNovaSenha" runat="server" CausesValidation="True" MaxLength="8" TextMode="Password" Width="137px"></asp:TextBox></center>
             </div>
        <br />
      <center>  <asp:Label ID="lbConfirmaSenha" runat="server" Font-Bold="True" Font-Names="Roboto Light" Text="Confirme a nova senha:"></asp:Label><asp:TextBox ID="tbConfirmarSenha" runat="server" CausesValidation="True" MaxLength="8" TextMode="Password" Width="137px"></asp:TextBox>
          <asp:CompareValidator ID="cvSenha" runat="server" ControlToCompare="tbConfirmarSenha" ControlToValidate="tbNovaSenha" EnableClientScript="False" ErrorMessage="*Senha não confere" Font-Bold="True" Font-Names="Roboto Light" Font-Size="XX-Small" ForeColor="#FF3300"></asp:CompareValidator>
        </center>
&nbsp;<center><asp:Button ID="btAtualizarSenha" runat="server" BackColor="#003366" BorderStyle="None" Font-Names="Roboto Light" ForeColor="White" OnClick="Button1_Click" Text="Alterar" Width="108px" /></center>
      <p>  <center>
          <asp:Label ID="LbAlerta" runat="server" Font-Size="Medium" ForeColor="#FF3300"></asp:Label>
          </center>
        </p>
    </form>
</body>
</html>
