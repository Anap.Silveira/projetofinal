﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace projetoFinal
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lbDoutor.Text = Request.QueryString["crm"];
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var connString = "Server=den1.mysql5.gear.host;Database=clinicatours;Uid=clinicatours;Password=Tj5t_EW-Ov8y";
            var connection = new MySqlConnection(connString);
            var command = connection.CreateCommand();


            try
            {
                connection.Open();


                if (tbNovaRua.Text.Length > 0 && tbNumero.Text.Length > 0 && tbNovoResidencial.Text.Length > 0 && tbNovoCelular.Text.Length > 0)
                {
                    command.CommandText = "UPDATE tb_logradouro join tb_medico on tb_medico.id_logradouro = tb_logradouro.id SET rua = '" + tbNovaRua.Text + "' , numero ='" + tbNumero.Text + "' WHERE tb_medico.crm like '" + lbDoutor.Text + "';" +
                        "UPDATE tb_medico set tel_residencial = '" + tbNovoResidencial.Text + "' tel_celular = '" + tbNovoCelular.Text + "'  where crm like '" + lbDoutor.Text + "'";
                    int rowAffected = command.ExecuteNonQuery();

                    if (rowAffected > 0)
                    {
                        LbAlerta.Text = "Dados atualizados";
                        Response.Redirect("Doutor.aspx");
                    }
                    else
                    {
                        LbAlerta.Text = "Os dados não foram atualizado, tente novamente!";
                    }

                }if (tbNovoCelular.Text.Length > 0)

                {
                    command.CommandText = "UPDATE tb_medico set tel_celular = '" + tbNovoCelular.Text + "' where crm like '" + lbDoutor.Text + "'";
                    int rowAffected = command.ExecuteNonQuery();

                    if (rowAffected > 0)
                    {
                        LbAlerta.Text = "Telefone Atualizado";
                        Response.Redirect("Doutor.aspx");
                    }
                    else
                    {
                        LbAlerta.Text = "O telefone não foi atualizado, tente novamente!";
                    }


                }if (tbNovoResidencial.Text.Length>0)
                {
                    command.CommandText = "UPDATE tb_medico set tel_residencial = '" + tbNovoResidencial.Text + "' where crm like '" + lbDoutor.Text + "'";
                    int rowAffected = command.ExecuteNonQuery();

                    if (rowAffected > 0)
                    {
                        LbAlerta.Text = "Telefone Atualizado";
                        Response.Redirect("Doutor.aspx");
                    }
                    else
                    {
                        LbAlerta.Text = "O telefone não foi atualizado, tente novamente!";
                    }

                }if (tbNovaRua.Text.Length >0 && tbNumero.Text.Length >0)
                {

                    command.CommandText = "UPDATE tb_logradouro join tb_medico on tb_medico.id_logradouro = tb_logradouro.id SET rua = '" +tbNovaRua.Text + "' , numero ='"+tbNumero.Text +"' WHERE tb_medico.crm like '" + lbDoutor.Text + "'";
                    int rowAffected = command.ExecuteNonQuery();

                    if (rowAffected > 0)
                    {
                        LbAlerta.Text = "Endereço atualizado";
                        Response.Redirect("Doutor.aspx");
                    }
                    else
                    {
                        LbAlerta.Text = "O endereço não foi atualizado, tente novamente!";
                    }

                }
                    
            }

            catch (Exception ex)
            {
                LbAlerta.Text = "Não foi possivel estabelecer conexão. \n" + ex.Message;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
        }
    }
}